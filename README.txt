

Complete 22 Mar 2017

To make: make

Instructions / controls: there are 18 useable controls 
	to manipulate the cube.
				   e/ \	  r
			s	/		  \ 	 f
			/					\ 
	   z/							  \		v
	/				white					\
	|	\								/	|
	| 7										|9
	|		  \					  /			|
	|										|
	| 4			   \		/				|6
	|										|
	|				   \/					|
	| 1				   |		orange		|3
	|		green							|
	\									  /
	t \				   |			  i	/
	    \							  /
		  \			   |		 	/
			\					j /
			g \		   |		/
			   	\			  /
				  \	   | n	/
				   b\	  / 
					  \ /
	
The controls are in groups of three.
E S Z perform rotations about the green-blue axis
E rotates the rear blue face of the cube by itself
Z rotates the near green face of the cube by itself
S flips the cube such that orange is on top, white
	moves to the back, and yellow is revealed from
	the bottom
I J N are inverse controls to E S Z respectively

Consider you keyboard like this 
q w    E R   T _ _ I   o p 		7 _ 9
 a 	  S _ F   G _ J	  k l ; 	4 _ 6  
	 Z _ _ V   B N   m , . 		1 _ 3
	 

