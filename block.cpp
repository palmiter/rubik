//Block.cpp
#ifndef __IOSTREAM
#define __IOSTREAM
#include <iostream>
#endif
#include "Block.hpp"

// double red []= {1.0,0.0,0.0};
// double orange [] = {1.0,0.5,0.0};
// double yellow [] = {1.0,1.0,0.0};
// double green [] = {0.0,1.0,0.0};
// double blue [] = {0.0,0.0,1.0};
// double white [] = {1.0,1.0,1.0};

		
block::block(std::string color0, std::string color1, std::string color2, int count){
	std::vector <std::string> tiles;
	tiles.push_back(std::string(color0));
	tiles.push_back(std::string(color1));
	tiles.push_back(std::string(color2));
	this->tiles = tiles;
	this->count = count;
	};
// copy constructors for different sized blocks

std::ostream& operator<<(std::ostream& os, const block& blo){
	os << blo.tiles[0] << "=color0 color1=" << blo.tiles[1] << " color2=" << blo.tiles[2];
	return os;
}  