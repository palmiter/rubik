// cube.cpp
// The idea here is to display a rubik's cube to understand how manipulating sides
//  changes positions of tiles.
// David Palmiter
// March 2017 updates

//spins about the top face
//able 	0	2	 20	  18  -	1	11	19	 9
//		0 1	0 2	 0 2  0	0	0 1	0 1	0 0	 0 0
//		1 2	1 1	 1 0  1 2	1 0	1 0	1 1	 1 1
//		2 0	2 0	 2 1  2 1 

//armor 6	8	 26	  24  -	7	17	25	 15
//		0 2 0 0	 0 1  0 1	0	0	0	 0
//		1 1	1 2	 1 2  1 2	1	1	1	 1
//		2 0	2 1	 2 0  2 0

//alpha	3	5	 23	  21	4	12	14	22
//		0 1	0 1	 0 1  0 1	
//		1 0	1 0	 1 0  1 0

//spins about the left face
//baker 0   18   24   6   - 3   9   21   15
//		0 2	0 2	 0 0  0 1	0 1	0 1	0 0	 0 0
//		1 1	1 0	 1 2  1	2	1 0	1 0	1 1	 1 1
//		2 0	2 1	 2 1  2 0 	

//bravo 2   20   26   8   - 5   11  23   17
//		0 0	0 1	 0 2  0 2	0 0	0 0	0 1	 0 1
//		1 2	1 2	 1 1  1 0	1 1	1 1	1 0	 1 0
//		2 1	2 0	 2 0  2 1 	

//beta 	1   19 	 25	  7		4	10	16	22
//		0 1	0 1	 0 1  0 1	
//		1 0	1 0	 1 0  1 0

//spins about the right face - all these have corresponding array assignments
//coldstone	0	2	8	6  -1	5	7	3
//charlie	18	20	26	24 -19	23	25	21
//gamma		9	11	17	15	10 	12	14	16
#include "Cube.hpp"



Cube::Cube(){
	std::vector<block> tileset;
	tileset.push_back(block("white","orange","green",3));//0
	tileset.push_back(block("white","green","black",2));//1
	tileset.push_back(block("red","white","green",3));//2
	tileset.push_back(block("orange","green","black",2));//3
	tileset.push_back(block("green","black","black",1));//4
	tileset.push_back(block("red","green","black",2));//5
	tileset.push_back(block("orange","yellow","green",3));//6
	tileset.push_back(block("yellow","green","black",2));//7
	tileset.push_back(block("yellow","red","green",3));//8
	tileset.push_back(block("white","orange","black",2));//9
	tileset.push_back( block("white","black","black",1));//10
	tileset.push_back( block("red","white","black",2));//11
	tileset.push_back( block("orange","black","black",2));//12
	tileset.push_back( block("aubergine","black","black",0));//13
	tileset.push_back( block("red","black","black",2));//14
	tileset.push_back( block("orange","yellow","black",2));//15
	tileset.push_back( block("yellow","black","black",1));//16
	tileset.push_back( block("yellow","red","black",2));//17
	tileset.push_back( block("white","orange","blue",3));//18
	tileset.push_back( block("white","blue","black",2));//19
	tileset.push_back( block("red","white","blue",3)); //20USA!USA!USA!
	tileset.push_back( block("orange","blue","black",2));//21
	tileset.push_back( block("blue","black","black",1));//22
	tileset.push_back( block("red","blue","black",2));//23
	tileset.push_back( block("orange","yellow","blue",3));//24
	tileset.push_back( block("yellow","blue","black",2));//25
	tileset.push_back( block("yellow","red","blue",3));//26
	this->tileset = tileset;
	color.insert(std::pair<std::string,std::vector<double> >("red",red));
	color.insert(std::pair<std::string,std::vector<double> >("orange",orange));
	color.insert(std::pair<std::string,std::vector<double> >("yellow",yellow));
	color.insert(std::pair<std::string,std::vector<double> >("green",green));
	color.insert(std::pair<std::string,std::vector<double> >("blue",blue));
	color.insert(std::pair<std::string,std::vector<double> >("white",white));
}

std::vector<std::string> Cube::generateFace(int face){
	std::vector<std::string> result;

	switch (face){
		case 0:
			for(int i =0; i<19; i+=9){
			result.push_back(tileset[i].tiles[0]);
			result.push_back(tileset[i+1].tiles[0]);
			result.push_back(tileset[i+2].tiles[1]);
				}
		break;
		case 1:
			for(int i = 0; i<19; i+=9){
			result.push_back(tileset[i].tiles[1]);
			result.push_back(tileset[i+3].tiles[0]);
			result.push_back(tileset[i+6].tiles[0]);
				}
		break;
		case 2:
			for(int i = 0; i<9; i+=3){
			int a = (i+1)%2;
			result.push_back(tileset[i].tiles[1+a]);
			result.push_back(tileset[i+1].tiles[0+a]);
			result.push_back(tileset[i+2].tiles[1+a]);
				}
			return result;
		break;
		}
	return result;
		}
void Cube::spin(int axis, bool sign, int layer){// Turns the whole cube to face the user in a different way.
	
	block temp = block("red","red","red",0);
		int a,b;
		switch (axis){
			case 0: // rotations about face 0 - a names
				switch(layer){
					case 0: //7 and 9
						temp.tiles[0] = tileset[18].tiles[0]; temp.tiles[1] = tileset[18].tiles[1]; temp.tiles[2]=tileset[18].tiles[2]; //3

						if (sign){
							a=20;
							b=0;
						tileset[18].tiles[2] = tileset[a].tiles[0];
						tileset[18].tiles[0] = tileset[a].tiles[1];
						tileset[18].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[2].tiles[0];
						tileset[a].tiles[1] = tileset[2].tiles[1];
						tileset[a].tiles[0] = tileset[2].tiles[2];
						
						tileset[2].tiles[1] = tileset[b].tiles[0];
						tileset[2].tiles[2] = tileset[b].tiles[1];
						tileset[2].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];//temp.tiles[0]; //"yellow" debug
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						else{
							a=0;
							b=20;						
						tileset[18].tiles[0] = tileset[a].tiles[0];
						tileset[18].tiles[2] = tileset[a].tiles[1];
						tileset[18].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[2].tiles[0];
						tileset[a].tiles[1] = tileset[2].tiles[2];
						tileset[a].tiles[0] = tileset[2].tiles[1];
						
						tileset[2].tiles[2] = tileset[b].tiles[0];
						tileset[2].tiles[1] = tileset[b].tiles[1];
						tileset[2].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[2];
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						}
												// shift layer 0 corners 20 0

						
// spins about the top face
// able 	0	2	 20	  18  -	1	11	19	 9
		// 0 1	0 2	 0 2  0	0	0 1	0 1	0 0	 0 0
		// 1 2	1 1	 1 0  1 2	1 0	1 0	1 1	 1 1
		// 2 0	2 0	 2 1  2 1 						
						
						temp.tiles[0]= tileset[9].tiles[0]; temp.tiles[1] = tileset[9].tiles[1];
						if (sign){
							a=19;
							b=1;}
						else{
							a=1;
							b=19;}
						// shift layer 0 edges //2
						
						tileset[9].tiles[0] = tileset[a].tiles[0];
						tileset[9].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[11].tiles[0];
						tileset[a].tiles[0] = tileset[11].tiles[1];
						
						tileset[11].tiles[1] = tileset[b].tiles[0];
						tileset[11].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
					return;
					break;
					case 1:
						temp.tiles[0] = tileset[18].tiles[0]; temp.tiles[1] = tileset[18].tiles[1]; temp.tiles[2]=tileset[18].tiles[2]; //3

						if (sign){
							a=20;
							b=0;
						tileset[18].tiles[2] = tileset[a].tiles[0];
						tileset[18].tiles[0] = tileset[a].tiles[1];
						tileset[18].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[2].tiles[0];
						tileset[a].tiles[1] = tileset[2].tiles[1];
						tileset[a].tiles[0] = tileset[2].tiles[2];
						
						tileset[2].tiles[1] = tileset[b].tiles[0];
						tileset[2].tiles[2] = tileset[b].tiles[1];
						tileset[2].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];//temp.tiles[0]; //"yellow" debug
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						else{
							a=0;
							b=20;						
						tileset[18].tiles[0] = tileset[a].tiles[0];
						tileset[18].tiles[2] = tileset[a].tiles[1];
						tileset[18].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[2].tiles[0];
						tileset[a].tiles[1] = tileset[2].tiles[2];
						tileset[a].tiles[0] = tileset[2].tiles[1];
						
						tileset[2].tiles[2] = tileset[b].tiles[0];
						tileset[2].tiles[1] = tileset[b].tiles[1];
						tileset[2].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[2];
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						}
						
						temp.tiles[0]= tileset[9].tiles[0]; temp.tiles[1] = tileset[9].tiles[1];
						if (sign){
							a=19;
							b=1;}
						else{
							a=1;
							b=19;}
						// shift layer 0 edges //2
						
						tileset[9].tiles[0] = tileset[a].tiles[0];
						tileset[9].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[11].tiles[0];
						tileset[a].tiles[0] = tileset[11].tiles[1];
						
						tileset[11].tiles[1] = tileset[b].tiles[0];
						tileset[11].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						
						
						temp.tiles[0] = tileset[21].tiles[0]; temp.tiles[1] = tileset[21].tiles[1]; //2
						if (sign){
							a=23;
							b=3;}
						else{
							a=3;
							b=23;}
						// shift layer 1 edges
						tileset[21].tiles[1] = tileset[a].tiles[0];
						tileset[21].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[5].tiles[0];
						tileset[a].tiles[0] = tileset[5].tiles[1];
						
						tileset[5].tiles[1] = tileset[b].tiles[0];
						tileset[5].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
// alpha	3	5	 23	  21	
		// 0 1	0 1	 0 1  0 1	
		// 1 0	1 0	 1 0  1 0
						temp.tiles[0] =  tileset[12].tiles[0]; //1
						if (sign){
							a=22;
							b=4;}
						else{
							a=4;
							b=22;}
						
						// shift A ortho faces 4 14 22 12 aka layer 1 faces
						tileset[12].tiles[0] = tileset[a].tiles[0];
						
						tileset[a].tiles[0] = tileset[14].tiles[0];
						
						tileset[14].tiles[0] = tileset[b].tiles[0];
						
						tileset[b].tiles[0] = temp.tiles[0];
	
//armor 6	8	 26	  24  -	7	17	25	 15
//		0 2 0 0	 0 1  0 1	0	0	0	 0
//		1 1	1 2	 1 2  1 2	1	1	1	 1
//		2 0	2 1	 2 0  2 0					
						temp.tiles[0] = tileset[24].tiles[0]; temp.tiles[1] = tileset[24].tiles[1]; temp.tiles[2] = tileset[24].tiles[2]; //3
						if (sign){
							a=26;
							b=6;
						tileset[24].tiles[1] = tileset[a].tiles[0];
						tileset[24].tiles[2] = tileset[a].tiles[1];
						tileset[24].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[8].tiles[0];
						tileset[a].tiles[2] = tileset[8].tiles[1];
						tileset[a].tiles[1] = tileset[8].tiles[2];

						
						tileset[8].tiles[2] = tileset[b].tiles[0];
						tileset[8].tiles[0] = tileset[b].tiles[1];
						tileset[8].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];
						}
						else{
							a=6;
							b=26;
						// shift layer 2 corners
						tileset[24].tiles[2] = tileset[a].tiles[0];
						tileset[24].tiles[1] = tileset[a].tiles[1];
						tileset[24].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[1] = tileset[8].tiles[0];
						tileset[a].tiles[2] = tileset[8].tiles[1];
						tileset[a].tiles[0] = tileset[8].tiles[2];
						
						tileset[8].tiles[0] = tileset[b].tiles[0]; 
						tileset[8].tiles[2] = tileset[b].tiles[1];
						tileset[8].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];//orange
						tileset[b].tiles[0] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						temp.tiles[0] = tileset[15].tiles[0]; temp.tiles[1] = tileset[15].tiles[1]; //2
						if (sign){
							a=25;
							b=7;}
						else{
							a=7;
							b=25;}
						// shift layer 2 edges
						tileset[15].tiles[1] = tileset[a].tiles[0];
						tileset[15].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[17].tiles[0];
						tileset[a].tiles[1] = tileset[17].tiles[1];
						
						tileset[17].tiles[0] = tileset[b].tiles[0];
						tileset[17].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
					return;
					break;
					case 2:
												temp.tiles[0] = tileset[24].tiles[0]; temp.tiles[1] = tileset[24].tiles[1]; temp.tiles[2] = tileset[24].tiles[2]; //3
						if (sign){
							a=26;
							b=6;
						tileset[24].tiles[1] = tileset[a].tiles[0];
						tileset[24].tiles[2] = tileset[a].tiles[1];
						tileset[24].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[8].tiles[0];
						tileset[a].tiles[2] = tileset[8].tiles[1];
						tileset[a].tiles[1] = tileset[8].tiles[2];

						
						tileset[8].tiles[2] = tileset[b].tiles[0];
						tileset[8].tiles[0] = tileset[b].tiles[1];
						tileset[8].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];
						}
						else{
							a=6;
							b=26;
						// shift layer 2 corners
						tileset[24].tiles[2] = tileset[a].tiles[0];
						tileset[24].tiles[1] = tileset[a].tiles[1];
						tileset[24].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[1] = tileset[8].tiles[0];
						tileset[a].tiles[2] = tileset[8].tiles[1];
						tileset[a].tiles[0] = tileset[8].tiles[2];
						
						tileset[8].tiles[0] = tileset[b].tiles[0]; 
						tileset[8].tiles[2] = tileset[b].tiles[1];
						tileset[8].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];//orange
						tileset[b].tiles[0] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						temp.tiles[0] = tileset[15].tiles[0]; temp.tiles[1] = tileset[15].tiles[1]; //2
						if (sign){
							a=25;
							b=7;}
						else{
							a=7;
							b=25;}
						// shift layer 2 edges
						tileset[15].tiles[1] = tileset[a].tiles[0];
						tileset[15].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[17].tiles[0];
						tileset[a].tiles[1] = tileset[17].tiles[1];
						
						tileset[17].tiles[0] = tileset[b].tiles[0];
						tileset[17].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
					return;
					break;
				}
			case 1: // rotations about face 1 - b names
				switch(layer){
					case 0:
						temp.tiles[0] = tileset[6].tiles[0]; temp.tiles[1] = tileset[6].tiles[1]; temp.tiles[2] = tileset[6].tiles[2]; //3
						if (sign){
							a=24;
							b=0;
						// shift layer 0 corners
						tileset[6].tiles[0] = tileset[a].tiles[0];
						tileset[6].tiles[2] = tileset[a].tiles[1];
						tileset[6].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[18].tiles[0];
						tileset[a].tiles[0] = tileset[18].tiles[1];
						tileset[a].tiles[1] = tileset[18].tiles[2];
						
						tileset[18].tiles[2] = tileset[b].tiles[0];
						tileset[18].tiles[1] = tileset[b].tiles[1];
						tileset[18].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];}
						else{
							a=0;
							b=24;
						// shift layer 0 corners
						tileset[6].tiles[2] = tileset[a].tiles[0];
						tileset[6].tiles[0] = tileset[a].tiles[1];
						tileset[6].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[18].tiles[0];
						tileset[a].tiles[1] = tileset[18].tiles[1];
						tileset[a].tiles[0] = tileset[18].tiles[2];
						
						tileset[18].tiles[1] = tileset[b].tiles[0];
						tileset[18].tiles[2] = tileset[b].tiles[1];
						tileset[18].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];}
						
// baker 0   18   24   6   - 3   9   21   15
		// 0 2	0 2	 0 0  0 1	0 1	0 1	0 0	 0 0
		// 1 1	1 0	 1 2  1	2	1 0	1 0	1 1	 1 1
		// 2 0	2 1	 2 1  2 0 						
						
						temp.tiles[0] = tileset[15].tiles[0]; temp.tiles[1] = tileset[15].tiles[1];
						// shift layer 0 edges //2
						if (sign){
							a=21;
							b=3;}
						else{
							a=3;
							b=21;}
						
						tileset[15].tiles[0] = tileset[a].tiles[0];
						tileset[15].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[9].tiles[0];
						tileset[a].tiles[0] = tileset[9].tiles[1];
						
						tileset[9].tiles[1] = tileset[b].tiles[0];
						tileset[9].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
					return;
					break;
					case 1:
						temp.tiles[0] = tileset[6].tiles[0]; temp.tiles[1] = tileset[6].tiles[1]; temp.tiles[2] = tileset[6].tiles[2]; //3
						if (sign){
							a=24;
							b=0;
						// shift layer 0 corners
						tileset[6].tiles[0] = tileset[a].tiles[0];
						tileset[6].tiles[2] = tileset[a].tiles[1];
						tileset[6].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[18].tiles[0];
						tileset[a].tiles[0] = tileset[18].tiles[1];
						tileset[a].tiles[1] = tileset[18].tiles[2];
						
						tileset[18].tiles[2] = tileset[b].tiles[0];
						tileset[18].tiles[1] = tileset[b].tiles[1];
						tileset[18].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];
						}
						else{
							a=0;
							b=24;
						// shift layer 0 corners
						tileset[6].tiles[2] = tileset[a].tiles[0];
						tileset[6].tiles[0] = tileset[a].tiles[1];
						tileset[6].tiles[1] = tileset[a].tiles[2];
						
						tileset[a].tiles[2] = tileset[18].tiles[0];
						tileset[a].tiles[1] = tileset[18].tiles[1];
						tileset[a].tiles[0] = tileset[18].tiles[2];
						
						tileset[18].tiles[1] = tileset[b].tiles[0];
						tileset[18].tiles[2] = tileset[b].tiles[1];
						tileset[18].tiles[0] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[2] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];}
						
						
						temp.tiles[0] = tileset[15].tiles[0]; temp.tiles[1] = tileset[15].tiles[1];
						// shift layer 0 edges //2
						if (sign){
							a=21;
							b=3;}
						else{
							a=3;
							b=21;}
						
						tileset[15].tiles[0] = tileset[a].tiles[0];
						tileset[15].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[9].tiles[0];
						tileset[a].tiles[0] = tileset[9].tiles[1];
						
						tileset[9].tiles[1] = tileset[b].tiles[0];
						tileset[9].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						
						temp.tiles[0] = tileset[7].tiles[0]; temp.tiles[1] = tileset[7].tiles[1]; //2
						if (sign){
							a=25;
							b=1;}
						else{
							a=1;
							b=25;}
						
						// shift layer 1 edges
						tileset[7].tiles[1] = tileset[a].tiles[0];
						tileset[7].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[1] = tileset[19].tiles[0];
						tileset[a].tiles[0] = tileset[19].tiles[1];
						
						tileset[19].tiles[1] = tileset[b].tiles[0];
						tileset[19].tiles[0] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];

// beta 	1   19 	 25	  7
		// 0 1	0 1	 0 1  0 1	
		// 1 0	1 0	 1 0  1 0
						if (sign){
							a=10;
							b=16;
							}
						else{
							a=16;
							b=10;
							} // 4 10 22 16
						temp.tiles[0] = tileset[4].tiles[0];
						
						tileset[4].tiles[0] = tileset[b].tiles[0];
						tileset[b].tiles[0] = tileset[22].tiles[0];
						tileset[22].tiles[0] = tileset[a].tiles[0];
						tileset[a].tiles[0] = temp.tiles[0];
							

						temp.tiles[0] = tileset[8].tiles[0]; temp.tiles[1] = tileset[8].tiles[1]; temp.tiles[2] = tileset[8].tiles[2]; //3
						if (sign){
							a=26;
							b=2;
							// shift layer 2 corners
						tileset[8].tiles[2] = tileset[a].tiles[0];
						tileset[8].tiles[1] = tileset[a].tiles[1];
						tileset[8].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[1] = tileset[20].tiles[0];
						tileset[a].tiles[2] = tileset[20].tiles[1];
						tileset[a].tiles[0] = tileset[20].tiles[2];
						
						tileset[20].tiles[0] = tileset[b].tiles[0];
						tileset[20].tiles[2] = tileset[b].tiles[1];
						tileset[20].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						else{
							a=2;
							b=26;
						// shift layer 2 corners
						tileset[8].tiles[1] = tileset[a].tiles[0];
						tileset[8].tiles[2] = tileset[a].tiles[1];
						tileset[8].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[20].tiles[0];
						tileset[a].tiles[2] = tileset[20].tiles[1];
						tileset[a].tiles[1] = tileset[20].tiles[2];
						
						tileset[20].tiles[2] = tileset[b].tiles[0];
						tileset[20].tiles[0] = tileset[b].tiles[1];
						tileset[20].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];
						}
						
						temp.tiles[0] = tileset[17].tiles[0]; temp.tiles[1] = tileset[17].tiles[1]; //2
						// shift layer 2 edges
						if (sign){
							a=23;
							b=5;}
						else{
							a=5;
							b=23;}
						tileset[17].tiles[1] = tileset[a].tiles[0];
						tileset[17].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[11].tiles[0];
						tileset[a].tiles[1] = tileset[11].tiles[1];
						
						tileset[11].tiles[0] = tileset[b].tiles[0];
						tileset[11].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
						
					return;
					break;
					case 2:
						temp.tiles[0] = tileset[8].tiles[0]; temp.tiles[1] = tileset[8].tiles[1]; temp.tiles[2] = tileset[8].tiles[2]; //3
						// shift layer 2 corners
						if (sign){
							a=26;
							b=2;
						// shift layer 2 corners
						tileset[8].tiles[2] = tileset[a].tiles[0];
						tileset[8].tiles[1] = tileset[a].tiles[1];
						tileset[8].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[1] = tileset[20].tiles[0];
						tileset[a].tiles[2] = tileset[20].tiles[1];
						tileset[a].tiles[0] = tileset[20].tiles[2];
						
						tileset[20].tiles[0] = tileset[b].tiles[0];
						tileset[20].tiles[2] = tileset[b].tiles[1];
						tileset[20].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
						tileset[b].tiles[1] = temp.tiles[2];
						}
						else{
							a=2;
							b=26;
						// shift layer 2 corners
						tileset[8].tiles[1] = tileset[a].tiles[0];
						tileset[8].tiles[2] = tileset[a].tiles[1];
						tileset[8].tiles[0] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[20].tiles[0];
						tileset[a].tiles[2] = tileset[20].tiles[1];
						tileset[a].tiles[1] = tileset[20].tiles[2];
						
						tileset[20].tiles[2] = tileset[b].tiles[0];
						tileset[20].tiles[0] = tileset[b].tiles[1];
						tileset[20].tiles[1] = tileset[b].tiles[2];
						
						tileset[b].tiles[2] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[0] = temp.tiles[2];
						}
						

// bravo 2   20   26   8   - 5   11  23   17
		// 0 0	0 1	 0 2  0 2	0 0	0 0	0 1	 0 1
		// 1 2	1 2	 1 1  1 0	1 1	1 1	1 0	 1 0
		// 2 1	2 0	 2 0  2 1 						
						
						temp.tiles[0] = tileset[17].tiles[0]; temp.tiles[1] = tileset[17].tiles[1]; //2
						// shift layer 2 edges
						if (sign){
							a=23;
							b=5;}
						else{
							a=5;
							b=23;}
						tileset[17].tiles[1] = tileset[a].tiles[0];
						tileset[17].tiles[0] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[11].tiles[0];
						tileset[a].tiles[1] = tileset[11].tiles[1];
						
						tileset[11].tiles[0] = tileset[b].tiles[0];
						tileset[11].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[1] = temp.tiles[0];
						tileset[b].tiles[0] = temp.tiles[1];
					
					return;	
					break;
				}
			case 2:
			{
				switch(layer){
					case 0:
						temp.tiles[0] = tileset[2].tiles[0]; temp.tiles[1] = tileset[2].tiles[1]; temp.tiles[2] = tileset[2].tiles[2]; //3
						if (sign){
							a=8;
							b=0;
						// shift layer 0 corners
						tileset[2].tiles[0] = tileset[a].tiles[0];
						tileset[2].tiles[1] = tileset[a].tiles[1];
						tileset[2].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[6].tiles[0];
						tileset[a].tiles[1] = tileset[6].tiles[1];
						tileset[a].tiles[2] = tileset[6].tiles[2];
						
						tileset[6].tiles[0] = tileset[b].tiles[0];
						tileset[6].tiles[1] = tileset[b].tiles[1];
						tileset[6].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
						}
						else{
							a=0;
							b=8;
						// shift layer 0 corners
						tileset[2].tiles[0] = tileset[a].tiles[0];
						tileset[2].tiles[1] = tileset[a].tiles[1];
						tileset[2].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[6].tiles[0];
						tileset[a].tiles[1] = tileset[6].tiles[1];
						tileset[a].tiles[2] = tileset[6].tiles[2];
						
						tileset[6].tiles[0] = tileset[b].tiles[0];
						tileset[6].tiles[1] = tileset[b].tiles[1];
						tileset[6].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
						}
						temp.tiles[1] = tileset[5].tiles[0]; temp.tiles[1] = tileset[5].tiles[1]; //2
						// shift layer 0 edges						
						if (sign){
							a=7;
							b=1;}
						else{
							a=1;
							b=7;}
						tileset[5].tiles[0] = tileset[a].tiles[0];
						tileset[5].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[3].tiles[0];
						tileset[a].tiles[1] = tileset[3].tiles[1];
						
						tileset[3].tiles[0] = tileset[b].tiles[0];
						tileset[3].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
					return;
					break;
					case 1:
						
						temp.tiles[0] = tileset[2].tiles[0]; temp.tiles[1] = tileset[2].tiles[1]; temp.tiles[2] = tileset[2].tiles[2]; //3
						if (sign){
							a=8;
							b=0;
						// shift layer 0 corners
						tileset[2].tiles[0] = tileset[a].tiles[0];
						tileset[2].tiles[1] = tileset[a].tiles[1];
						tileset[2].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[6].tiles[0];
						tileset[a].tiles[1] = tileset[6].tiles[1];
						tileset[a].tiles[2] = tileset[6].tiles[2];
						
						tileset[6].tiles[0] = tileset[b].tiles[0];
						tileset[6].tiles[1] = tileset[b].tiles[1];
						tileset[6].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
						}
						else{
							a=0;
							b=8;
						// shift layer 0 corners
						tileset[2].tiles[0] = tileset[a].tiles[0];
						tileset[2].tiles[1] = tileset[a].tiles[1];
						tileset[2].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[6].tiles[0];
						tileset[a].tiles[1] = tileset[6].tiles[1];
						tileset[a].tiles[2] = tileset[6].tiles[2];
						
						tileset[6].tiles[0] = tileset[b].tiles[0];
						tileset[6].tiles[1] = tileset[b].tiles[1];
						tileset[6].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
						}
						temp.tiles[1] = tileset[5].tiles[0]; temp.tiles[1] = tileset[5].tiles[1]; //2
						// shift layer 0 edges						
						if (sign){
							a=7;
							b=1;}
						else{
							a=1;
							b=7;}
						tileset[5].tiles[0] = tileset[a].tiles[0];
						tileset[5].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[3].tiles[0];
						tileset[a].tiles[1] = tileset[3].tiles[1];
						
						tileset[3].tiles[0] = tileset[b].tiles[0];
						tileset[3].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						// C ortho faces 10 12 16 14
						

						temp.tiles[0] = tileset[14].tiles[0];					
						if (sign){
							a=16;
							b=10;}
						else{
							a=10;
							b=16;}
						tileset[14].tiles[0] = tileset[a].tiles[0];
						
						tileset[a].tiles[0] = tileset[12].tiles[0];
						
						tileset[12].tiles[0] = tileset[b].tiles[0];
						
						tileset[b].tiles[0] = temp.tiles[0];
						
						//9 11 17 15
						temp.tiles[0] = tileset[9].tiles[0];	temp.tiles[1] = tileset[9].tiles[1];					
						if (sign){
							a=11;
							b=15;}
						else{
							a=15;
							b=11;}
						tileset[9].tiles[0] = tileset[a].tiles[0];
						tileset[9].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[17].tiles[0];
						tileset[a].tiles[1] = tileset[17].tiles[1];
						
						tileset[17].tiles[0] = tileset[b].tiles[0];
						tileset[17].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						
						temp.tiles[0] = tileset[20].tiles[0]; temp.tiles[1] = tileset[20].tiles[1]; temp.tiles[2] = tileset[24].tiles[2]; //3
						// shift layer 2 corners					
						if (sign){
							a=26;
							b=18;}
						else{
							a=18;
							b=26;}
						tileset[20].tiles[0] = tileset[a].tiles[0];
						tileset[20].tiles[1] = tileset[a].tiles[1];
						tileset[20].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[24].tiles[0];
						tileset[a].tiles[1] = tileset[24].tiles[1];
						tileset[a].tiles[2] = tileset[24].tiles[2];
						
						tileset[24].tiles[0] = tileset[b].tiles[0];
						tileset[24].tiles[1] = tileset[b].tiles[1];
						tileset[24].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
										
						temp.tiles[0] = tileset[23].tiles[0]; temp.tiles[1] = tileset[23].tiles[1]; //2
						// shift layer 2 edges		
						if (sign){
							a=25;
							b=19;}
						else{
							a=19;
							b=25;}
						tileset[23].tiles[0] = tileset[a].tiles[0];
						tileset[23].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[21].tiles[0];
						tileset[a].tiles[1] = tileset[21].tiles[1];
						
						tileset[21].tiles[0] = tileset[b].tiles[0];
						tileset[21].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
					return;
					break;
					case 2:
						temp.tiles[0] = tileset[20].tiles[0]; temp.tiles[1] = tileset[20].tiles[1]; temp.tiles[2] = tileset[24].tiles[2]; //3
						// shift layer 2 corners					
						if (sign){
							a=26;
							b=18;}
						else{
							a=18;
							b=26;}
						tileset[20].tiles[0] = tileset[a].tiles[0];
						tileset[20].tiles[1] = tileset[a].tiles[1];
						tileset[20].tiles[2] = tileset[a].tiles[2];
						
						tileset[a].tiles[0] = tileset[24].tiles[0];
						tileset[a].tiles[1] = tileset[24].tiles[1];
						tileset[a].tiles[2] = tileset[24].tiles[2];
						
						tileset[24].tiles[0] = tileset[b].tiles[0];
						tileset[24].tiles[1] = tileset[b].tiles[1];
						tileset[24].tiles[2] = tileset[b].tiles[2];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
						tileset[b].tiles[2] = temp.tiles[2];
										
						temp.tiles[0] = tileset[23].tiles[0]; temp.tiles[1] = tileset[23].tiles[1]; //2
						// shift layer 2 edges		
						if (sign){
							a=25;
							b=19;}
						else{
							a=19;
							b=25;}
						tileset[23].tiles[0] = tileset[a].tiles[0];
						tileset[23].tiles[1] = tileset[a].tiles[1];
						
						tileset[a].tiles[0] = tileset[21].tiles[0];
						tileset[a].tiles[1] = tileset[21].tiles[1];
						
						tileset[21].tiles[0] = tileset[b].tiles[0];
						tileset[21].tiles[1] = tileset[b].tiles[1];
						
						tileset[b].tiles[0] = temp.tiles[0];
						tileset[b].tiles[1] = temp.tiles[1];
					return;
						
					break;
				}
			}
		}
}


void checkCorrectness(); // Determines if there are any tiles out of place.
