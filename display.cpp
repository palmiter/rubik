/*
 * 
 */


 
#ifdef _WIN32
   #include <windows.h>

   void sleep(unsigned milliseconds)
   {
       Sleep(milliseconds);
   }
#else
    #include <unistd.h>

    void msleep(unsigned milliseconds)
    {
        usleep(milliseconds * 1000); // takes microseconds
    }
#endif
#include <GL/glut.h>
#include <iostream>
#include "Cube.cpp"

double r,g,b;
Cube rubik = Cube();
int k=1;



void displayFace (int face){
	std::vector <std::string> colorList = rubik.generateFace(face);
	int vertexArray [4][4][2];
	int x,y;

	switch (face){
		case 0:
			for (int i = 0; i < 4; i++){
				x= 325+100*i;
				y= 325+50*i;
				for (int j = 0; j < 4; j++){
					vertexArray[i][j][0]=x;
					vertexArray[i][j][1]=y;			
							// glColor3f(5e-1,5e-1,5e-1);
							// glBegin(GL_POLYGON);			
							// glVertex2i(x,y);			/* specify each vertex of triangle */
							// glVertex2i(x+8,y);
							// glVertex2i(x+8,y+8);
							// glVertex2i(x,y+8);
							// glEnd();
					y += 50;
					x -= 100;
					k+=10;
					}
				}
		break;
		case 1:
			for (int i = 0; i < 4; i++){
				x= 325+100*i;
				y= 325+50*i;
				for (int j = 0; j < 4; j++){
					vertexArray[i][j][0]=x;
					vertexArray[i][j][1]=y;
							// glColor3f(5e-1,5e-1,5e-1);
							// glBegin(GL_POLYGON);			
							// glVertex2i(x,y);			/* specify each vertex of triangle */
							// glVertex2i(x+8,y);
							// glVertex2i(x+8,y+8);
							// glVertex2i(x,y+8);
							// glEnd();
					y-=100;
					k+=50;
					}
			}					
		break;
		case 2: 
			for (int i = 0; i < 4; i++){
				x= 325;
				y= 325-100*i;
				for (int j = 0; j < 4; j++){
					vertexArray[i][j][0]=x;
					vertexArray[i][j][1]=y;
							// glColor3f(5e-1,0,5e-1);
							// glBegin(GL_POLYGON);			
							// glVertex2i(x,y);			/* specify each vertex of triangle */
							// glVertex2i(x+4,y);
							// glVertex2i(x+4,y+4);
							// glVertex2i(x,y+4);
							// glEnd();
					x -= 100;
					y += 50;}
			}
		break;
	glFlush();
	}
	for (int i = 0; i < 3; i++){
		for (int j = 0; j < 3; j++){
			std::vector<double> currentColor = (rubik.color.find(colorList[3*i+j]))->second;
			glColor3f(currentColor[0],currentColor[1],currentColor[2]);	
			glBegin(GL_POLYGON);			
			glVertex2i(vertexArray[i][j][0],vertexArray[i][j][1]);			/* specify each vertex of triangle */
			glVertex2i(vertexArray[i+1][j][0],vertexArray[i+1][j][1]);			/* specify each vertex of triangle */
			glVertex2i(vertexArray[i+1][j+1][0],vertexArray[i+1][j+1][1]);			/* specify each vertex of triangle */
			glVertex2i(vertexArray[i][j+1][0],vertexArray[i][j+1][1]);
			glEnd();
			// std::cout <<vertexArray[i][j][0] << " " << vertexArray[i][j][1] << std::endl;/* specify each vertex of triangle */
			// std::cout <<vertexArray[i+1][j][0]<< " " << vertexArray[i+1][j][1] << std::endl;			/* specify each vertex of triangle */
			// std::cout <<vertexArray[i+1][j+1][0]<< " " << vertexArray[i+1][j+1][1] << std::endl;			/* specify each vertex of triangle */
			// std::cout <<vertexArray[i][j+1][0]<< " " << vertexArray[i][j+1][1] << std::endl;

		}
	}
}


void displayCB(){
	for (int i=0; i<3;i++)
		displayFace(i);
	glColor3f(1.0,1.0,0.0);
	glFlush();	
}

void keyCB(unsigned char key, int x, int y)	/* called on key press */
{
  if( key == 'q' ) {exit(0);}
  else if (key == 'r'){
		rubik.spin(1,true,2);
		displayCB();
		}
  else if (key == 'f'){
		rubik.spin(1,true,1);
		displayCB();
		}
  else if (key == 'v'){
		rubik.spin(1,true,0);
		displayCB();
		}
  else if (key == 'e'){
		rubik.spin(2,false,2);
		displayCB();
		}
  else if (key == 's'){
		rubik.spin(2,false,1);
		displayCB();
		}
  else if (key == 'z'){
		rubik.spin(2,false,0);
		displayCB();
		}
  else if (key == '7'){
		rubik.spin(0,true,0);
		displayCB();
		}
  else if (key == '4'){
		rubik.spin(0,true,1);
		displayCB();
		}
  else if (key == '1'){
		rubik.spin(0,true,2);
		displayCB();
		}
  else if (key == '9'){
		rubik.spin(0,false,0);
		displayCB();
		}
  else if (key == '6'){
		rubik.spin(0,false,1);
		displayCB();
		}
  else if (key == '3'){
		rubik.spin(0,false,2);
		displayCB();
		}
  else if (key == 't'){
		rubik.spin(1,false,2);
		displayCB();
		}
  else if (key == 'g'){
		rubik.spin(1,false,1);
		displayCB();
		}
  else if (key == 'b'){
		rubik.spin(1,false,0);
		displayCB();
		}
  else if (key == 'n'){
		rubik.spin(2,true,0);
		displayCB();
		}
  else if (key == 'j'){
		rubik.spin(2,true,1);
		displayCB();
		}
  else if (key == 'i'){
		rubik.spin(2,true,2);
		displayCB();
		}
}




int main(int argc, char *argv[]){
  int win;
  r=0;
  g=0;
  b=0;

  glutInit(&argc, argv);		/* initialize GLUT system */

  glutInitDisplayMode(GLUT_RGB);
  glutInitWindowSize(700,700);		/* width=400pixels height=500pixels */
  win = glutCreateWindow("Rubik");	/* create window */

  /* from this point on the current window is win */

  glClearColor(0.0,0.0,0.0,1.0);	/* set background to black */
  gluOrtho2D(0,700,0,700);		/* how object is mapped to window */
  glutDisplayFunc(displayCB);		/* set window's display callback */
  glutKeyboardFunc(keyCB);		/* set window's key callback */

  glutMainLoop();			/* start processing events... */

  /* execution never reaches this point */

  return 0;
}
