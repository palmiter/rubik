// rubik.hpp
// The idea here is to display a rubik's cube to understand how manipulating sides
//  changes positions of tiles.
// David Palmiter
// 7 Dec 2014

#include <iostream>
#include <cstdio>
#include <ctype.h>

#define RandNum "rlud"

#define CLEAR "\033[0m"
#define RED "\e[101m"
#define YELLOW "\e[103m"
#define GREEN "\e[102m"
#define BLUE "\e[44m"
#define WHITE "\e[107m"
#define ORANGE "\e[105m"


char TOP[3][3] {{'a','b','c'},{'d','e','f'},{'g','h','i'}};
char BOTTOM[3][3] {{'s','t','u'},{'v','w','x'},{'y','z','0'}};
char FRONT[3][3] {{'j','k','l'},{'m','n','o'},{'p','q','r'}};
char BACK[3][3] {{'A','B','C'},{'D','E','F'},{'G','H','I'}};
char LEFT[3][3] {{'J','K','L'},{'M','N','O'},{'P','Q','R'}};
char RIGHT[3][3] {{'S','T','U'},{'V','W','X'},{'Y','Z','1'}};

void displayCube(); // Principal method for showing the appearance of the cube.

char getInput(); // Get user input to determine how to manipulate the cube.

void copyFaces();

void scramble();

void initCube();

void rotateFace(char edgeDirection); //Rotates a single face relative to the other sections.

void spin(int edge); // Turns the whole cube to face the user in a different way.

void checkCorrectness(); // Determines if there are any tiles out of place.
