//Block.hpp
#ifndef __IOSTREAM
#define __IOSTREAM
#include <iostream>
#endif

class block {
	public:
		block(std::string color0, std::string color1, std::string color2, int count);
		std::vector<std::string> tiles;
		int count=0;
		friend std::ostream& operator<<(std::ostream& os, const block& blo);
};
