/*
 * triangle.c -- A simple example of OpenGL and GLUT.
 */

#include <GL/glut.h>

void displayCB()		/* function called whenever redisplay needed */
{
  glClear(GL_COLOR_BUFFER_BIT);		/* clear the display */
  glColor3f(1.0, 1.0, 0.0);		/* set current color to white */
  glBegin(GL_POLYGON);			/* draw filled triangle */
  glVertex2i(50,50);			/* specify each vertex of triangle */
  glVertex2i(150,50);
  glVertex2i(150,150);
  glVertex2i(50,150);
  glEnd();				/* OpenGL draws the filled triangle */
  glColor3f(0.0, 1.0, 1.0);		/* set current color to white */
  glBegin(GL_POLYGON);			/* draw filled square*/	
  glVertex2i(250,250);			
  glVertex2i(350,250);
  glVertex2i(350,350);
  glVertex2i(250,350);
  glEnd();			
  glEnd();	
  glFlush();	
}

void keyCB(unsigned char key, int x, int y)	/* called on key press */
{
  if( key == 'q' ) exit(0);
  
}


int main(int argc, char *argv[])
{
  int win;
  

  glutInit(&argc, argv);		/* initialize GLUT system */

  glutInitDisplayMode(GLUT_RGB);
  glutInitWindowSize(600,600);		/* width=400pixels height=500pixels */
  win = glutCreateWindow("Rubik");	/* create window */

  /* from this point on the current window is win */

  glClearColor(0.0,0.0,0.0,1.0);	/* set background to black */
  gluOrtho2D(0,600,0,600);		/* how object is mapped to window */
  glutDisplayFunc(displayCB);		/* set window's display callback */
  glutKeyboardFunc(keyCB);		/* set window's key callback */

  glutMainLoop();			/* start processing events... */

  /* execution never reaches this point */

  return 0;
}
