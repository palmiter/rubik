//Tile.cpp

#include "Tile.hpp"
Tile::Tile (std::String lor, char de):color(lor), code(de){}

char getCode(){
	return this.code;
	}
std::String getColor(){
	return this.color;
	}

friend std::ostream& operator<<(std::ostream& o, Tile const& t){
	o << t.getColor() << " " << t.getCode();
	return o;
	}

