// Cube.hpp
// The idea here is to display a rubik's cube to understand how manipulating sides
//  changes positions of tiles.
// David Palmiter
// 7 Dec 2014

#include <iostream>
#include <vector>
#include <map>
#include <iterator>
#include "Block.cpp"

// Determines if there are any tiles out of place.

class Cube {
	public:
		Cube();
		std:: vector <block> tileset;
		bool checkCorrectness(); 
		void spin(int axis, bool sign, int layer);
		std::vector<std::string> generateFace(int face);
		
		std::map<std::string,std::vector<double> > color;
		
		std::vector<double> red = {1.0,0.0,0.0};
		std::vector<double> green = {0.0,1.0,0.0};
		std::vector<double> blue= {0.0,0.0,1.0};
		std::vector<double> yellow = {1.0,1.0,0.0};
		std::vector<double> orange = {1.0,0.5,0.0};
		std::vector<double> white = {1.0,1.0,1.0};
};
