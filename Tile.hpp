//Tile.hpp
#ifndef __IOSTREAM
#define __IOSTREAM
#include <iostream>
#endif
#ifndef __TILE_HPP
#define __TILE_HPP
class Tile {
	public:
		Tile(std:String color, char code);
		std::String getColor();
		char getCode();
		friend std::ostream& operator<<(std::ostream& o, Tile const& t);
	private:
		std::String color;
		char code;
}


#endif