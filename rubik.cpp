// rubik.cpp
// The idea here is to display a rubik's cube to understand how manipulating
//  sides changes positions of tiles.
// David Palmiter
// 7 Dec 2014

#include "rubik.hpp"
#include <stdlib.h>

//char TOP[3][3] {{'a','b','c'},{'d','e','f'},{'g','h','i'}};
//char BOTTOM[3][3] {{'s','t','u'},{'v','w','x'},{'y','z','0'}};
//char FRONT[3][3] {{'j','k','l'},{'m','n','o'},{'p','q','r'}};
//char BACK[3][3] {{'A','B','C'},{'D','E','F'},{'G','H','I'}};
//char LEFT[3][3] {{'J','K','L'},{'M','N','O'},{'P','Q','R'}};
//char RIGHT[3][3] {{'S','T','U'},{'V','W','X'},{'Y','Z','1'}};


char copyTop[3][3];
char copyRight[3][3];
char copyFront[3][3];
char copyLeft[3][3];
char copyBack[3][3];
char copyBottom[3][3];

char cube[6][3][3] ;

void initcube(){
	/*sets up the cube to its starting state, can also be used to reset the cube*/
	int i;
	int j;
	int k;

	for (k=0;k<6;k++){
		for (i=0;i<3; i++){	
			for (j=0;j<3; j++){
				if(k==0)
					cube[k][i][j] = TOP[i][j];
				if(k==1)
					cube[k][i][j] = FRONT[i][j];
				if(k==2)
					cube[k][i][j] = RIGHT[i][j];
				if(k==3)
					cube[k][i][j] = LEFT[i][j];
				if(k==4)
					cube[k][i][j] = BACK[i][j];
				if(k==5)
					cube[k][i][j] = BOTTOM[i][j];
			}
		}
	}
	
}

void displaycube(){
	int i;
	int j;
	int k;
	std::cout<<std::endl;
	for (k=0;k<6;k++){
		for (i=0;i<3; i++){	
			for (j=0;j<3; j++){
				if(cube[k][i][j] <= 'i' && cube[k][i][j] > 'Z') 							//red top
					std::cout << RED << cube[k][i][j] << CLEAR;
				
				else if(cube[k][i][j] >= 'j' && cube[k][i][j] <= 'r' && cube[k][i][j] > 'Z') //yellow front
					std::cout << YELLOW << cube[k][i][j] << CLEAR;
				
				else if(cube[k][i][j] >= 's' && cube[k][i][j] <= 'z' || cube[k][i][j] == '0') //orange bottom
					std::cout << ORANGE << cube[k][i][j] << CLEAR;
	
				else if(cube[k][i][j] >= 'A' && cube[k][i][j] <= 'I' && cube[k][i][j] < 'z') //White back
					std::cout << WHITE << cube[k][i][j] << CLEAR;
				
				else if(cube[k][i][j] >= 'S' && cube[k][i][j] <= 'Z' && cube[k][i][j] < 'a' || cube[k][i][j] == '1' ) // blue right
					std::cout << BLUE << cube[k][i][j] << CLEAR;
				
				else if(cube[k][i][j] >= 'J' && cube[k][i][j] <= 'R' && cube[k][i][j] < 'a') // green left
					std::cout << GREEN << cube[k][i][j] << CLEAR;
			}	
		
			std::cout << std::endl;
		}	
		std::cout<<std::endl;
	}
}
void copyFaces(){
	/*copies the current faces before rotating.*/
	int i,j;

	for (i=0;i<3; i++){	
		for (j=0;j<3; j++){
			//assign temp variables
			copyTop[i][j] = cube[0][i][j];
			copyFront[i][j] = cube[1][i][j];
			copyRight[i][j] = cube[2][i][j];
			copyLeft[i][j] = cube[3][i][j];
			copyBack[i][j] = cube[4][i][j];	
			copyBottom[i][j] = cube[5][i][j];					
		}	
	}
}
void rotateFace(char face){
	/*rotate cube in given direction -> 1 = up
										2 = down
										3 = left
										4 = right
	*/
	int i,j;
	face = tolower(face);
	copyFaces();
	for (i=0;i<3; i++){	
		for (j=0;j<3; j++){
			if(face == 'u'){    //rotate up
				cube[0][i][j] = copyFront[i][j];	
				cube[1][i][j] = copyBottom[i][j];	
				cube[4][i][j] = copyTop[i][j];
				cube[5][i][j] = copyBack[i][j];
			}
			if(face == 'd'){    //rotate down
				cube[0][i][j] = copyBack[i][j];	
				cube[1][i][j] = copyTop[i][j];	
				cube[4][i][j] = copyBottom[i][j];
				cube[5][i][j] = copyFront[i][j];
			}
			if(face == 'l'){    //rotate left
				cube[1][i][j] = copyRight[i][j];	
				cube[2][i][j] = copyBack[i][j];	
				cube[3][i][j] = copyFront[i][j];
				cube[4][i][j] = copyLeft[i][j];
			}
			if(face == 'r'){    //rotate right
				cube[1][i][j] = copyLeft[i][j];	
				cube[2][i][j] = copyFront[i][j];	
				cube[3][i][j] = copyBack[i][j];
				cube[4][i][j] = copyRight[i][j];
			}			
		}
	}
}
void spin(int edge, char direction){
	/*spin a row (left or right) or column(up or down) */
	int i,j,k;
	copyFaces();
	direction = tolower(direction);
	if(isdigit(edge)){
		std::cout<<"The chosen edge is not a integer"<<std::endl;
	}
	for(i=0; i<3;i++){
		for(j=0; j<3;j++){
			if (edge == 1 && direction == 'r'){  //top row ->
				cube[1][0][j] = copyLeft[0][j]; // new front
				cube[2][0][j] = copyFront[0][j]; //new right
				cube[3][0][j] = copyBack[0][j]; //new left
				cube[4][0][j] = copyRight[0][j]; //new back4
				
			}
			else if (edge == 1 && direction == 'l'){  //top row <-
				cube[1][0][j] = copyRight[0][j]; // new front
				cube[2][0][j] = copyBack[0][j]; //new right
				cube[3][0][j] = copyFront[0][j]; //new left
				cube[4][0][j] = copyLeft[0][j]; //new back
				
			}
			else if (edge == 2 && direction == 'r'){  //bottom row ->
				cube[1][2][j] = copyRight[2][j]; // new front
				cube[2][2][j] = copyBack[2][j]; //new right
				cube[3][2][j] = copyFront[2][j]; //new left
				cube[4][2][j] = copyLeft[2][j]; //new back
				
			}			
			else if (edge == 2 && direction == 'l'){  //bottom row <-
				cube[1][2][j] = copyRight[2][j]; // new front
				cube[2][2][j] = copyBack[2][j]; //new right
				cube[3][2][j] = copyFront[2][j]; //new left
				cube[4][2][j] = copyLeft[2][j]; //new back
				
			}
			else if (edge == 3 && direction == 'd' ){ //left column down
				cube[0][i][0] = copyBack[i][0]; // new top
				cube[1][i][0] = copyTop[i][0]; //new front
				cube[4][i][0] = copyBottom[i][0]; //new back
				cube[5][i][0] = copyFront[i][0]; //new bottom
					
			}			
			else if (edge == 3 && direction == 'u'){ //left column up
				cube[0][i][0] = copyFront[i][0]; // new top
				cube[1][i][0] = copyBottom[i][0]; //new front
				cube[4][i][0] = copyTop[i][0]; //new back
				cube[5][i][0] = copyBack[i][0]; //new bottom
				
			}			
			else if (edge == 4 && direction == 'd'){ //right column down
				cube[0][i][2] = copyBack[i][2]; // new top
				cube[1][i][2] = copyTop[i][2]; //new front
				cube[4][i][2] = copyBottom[i][2]; //new back
				cube[5][i][2] = copyFront[i][2]; //new bottom
				
			}			
			else if (edge == 4 && direction == 'u'){ //right column up
				cube[0][i][2] = copyFront[i][2]; // new top
				cube[1][i][2] = copyBottom[i][2]; //new front
				cube[4][i][2] = copyTop[i][2]; //new back
				cube[5][i][2] = copyBack[i][2]; //new bottom
				
			}
			else
				std::cout<<"Invalid Input"<<std::endl;
		}
	}			
} 

void scramble(){
		int runs;
		int edge;
		char ranDir;
		
		std::cout<< "How many scramblings: ";
		std::cin>>runs;
		
		for(runs = 1; runs < 10; runs++){
			int edge = rand()%4+1;
			ranDir =  RandNum[rand() % sizeof(RandNum)];
			if((edge == 1 || edge == 2) && (ranDir == 'l' || ranDir == 'r'))
					spin(edge,ranDir);
			
			else if((edge == 3 || edge == 4) && (ranDir == 'd' || ranDir == 'u'))
					spin(edge,ranDir);
			
				//spin(edge,ranDir);
		}
		displaycube();
}

char getInput(){
	/* Get user input to determine how to manipulate the cube.*/
	char input;	
	while(input != 'e'){
		std::cout<<"what would like to do:\nr:rotate\ns:spin\nm: scramble\ne:end"<<std::endl;
		std::cin>>input;
		if(input == 'r'){
			char rotation;
			std::cout<<"How would like to rotate?(u=up,d=down,l=left,r=right)"<<std::endl;
			std::cin>>rotation;
			rotateFace(rotation);
			displaycube();
		}
		else if(input == 's'){
			int square;
			char direction;
			std::cout<<"Enter a row or column folowed by a direction:\n1:top row 2:bottom row (both go left[l] or right[r]\n3:left column 4:right colunm (both go up[u] or down[d]"<<std::endl;
			std::cin>>square>>direction;
			spin(square, tolower(direction));
			displaycube();
		}
		else if(input == 'm'){
			scramble();
		}
		else if(input != 'e')
			std::cout<<"INVALID INPUT\n\n"<<std::endl;

	}
	
} 

int main(){
	initcube();
	displaycube();
	getInput();
	return 0;
}
