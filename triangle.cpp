/*
 * triangle.c -- A simple example of OpenGL and GLUT.
 */

#include <GL/glut.h>

void displayCB()		/* function called whenever redisplay needed */
{
 int TWOFACEX [][4] = 
 {{150, 250, 350, 450},
 {130, 243, 357, 470},
 {90,  230, 370, 510},
 {25,  208, 392, 575},
 {90,  230, 370, 510},
 {130, 243, 357, 470},
 {150, 250, 350, 450}};
 int TWOFACEY [] = {25,69,157,300,443,531,575};
			/* OpenGL draws the filled triangle */
  
  for (int i=0; i<2; i++){ //navigate through the two faces
		for(int y=0; y < 3; y++){ //rows
			for (int x = 0; x < 3; x++){
			  glColor3f(.2*x, .2*y, .2*i);
			  glBegin(GL_POLYGON);			
			  glVertex2i(TWOFACEX[y+3*i][x],TWOFACEY[y+3*i]);			/* specify each vertex of triangle */
			  glVertex2i(TWOFACEX[y+3*i][x+1],TWOFACEY[y+3*i]);
			  glVertex2i(TWOFACEX[y+3*i+1][x+1],TWOFACEY[y+3*i+1]);
			  glVertex2i(TWOFACEX[y+3*i+1][x],TWOFACEY[y+3*i+1]);
			  glEnd();	
			  glFlush();	
			    }}}
}

void keyCB(unsigned char key, int x, int y)	/* called on key press */
{
  if( key == 'q' ) exit(0);
  
}


int main(int argc, char *argv[])
{
  int win;
  

  glutInit(&argc, argv);		/* initialize GLUT system */

  glutInitDisplayMode(GLUT_RGB);
  glutInitWindowSize(600,600);		/* width=400pixels height=500pixels */
  win = glutCreateWindow("Rubik");	/* create window */

  /* from this point on the current window is win */

  glClearColor(0.0,0.0,0.0,1.0);	/* set background to black */
  gluOrtho2D(0,600,0,600);		/* how object is mapped to window */
  glutDisplayFunc(displayCB);		/* set window's display callback */
  glutKeyboardFunc(keyCB);		/* set window's key callback */

  glutMainLoop();			/* start processing events... */

  /* execution never reaches this point */

  return 0;
}
